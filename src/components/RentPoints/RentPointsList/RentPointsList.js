import React, { Component } from 'react';
import { Card, CardBody, CardTitle} from 'reactstrap';

class RentPointsList extends Component {
  render(){
    return (
      <div>
      {this.props.rentPoints.map((rentPoint, id) => (
					 	<div key={id} className="col-4 mb-4">
             <Card>
								<CardBody>
							 		<CardTitle>{rentPoint.name}</CardTitle>
						 			</CardBody>
						 			<CardBody>
                    <div>
                      <p>
                        Telephone Number : {rentPoint.contactInfo}
                      </p>
                      </div>
                      <div>
                        Working hours: I-V, {rentPoint.workingHours}
                      </div>
						 			</CardBody>
					 			</Card>
				 		</div>
					))}
      </div>
    )
  }
}

export default RentPointsList;