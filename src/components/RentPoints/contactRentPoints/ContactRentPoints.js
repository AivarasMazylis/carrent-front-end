import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import axios from 'axios';

class ContactRentPoints extends Component {
  handleSubmit(e){
    e.preventDefault();
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const message = document.getElementById('message').value;
    axios({
        method: "POST", 
        url:"https://car-rent-backend.herokuapp.com/send", 
        data: {
            name: name, 
            email: email,  
            message: message
        }
    }).then((response)=>{
        if (response.data.msg === 'success'){
            alert("Message Sent."); 
            this.resetForm()
        }else if(response.data.msg === 'fail'){
          console.log(response.data)
            alert("Message failed to send.")
        }
    })
}
  resetForm(){
    document.getElementById('contact-form').reset();
  }
  render() {
    return (
      <div className="container mt-5">
      <Form className="col-12" id="contact-form" onSubmit={this.handleSubmit.bind(this)} method="POST">
      <div className="row">
      <div className="col-6">
         <FormGroup row>
          <Label for="name" sm={2}>Name</Label>
          <Col sm={10}>
            <Input type="name" name="name" id="name" placeholder="Enter you're Name" />
          </Col>
        </FormGroup>
        </div>
        <div className="col-6">
        <FormGroup row>
          <Label for="email" sm={2}>Email</Label>
          <Col sm={10}>
            <Input type="email" name="email" id="email" placeholder="Email Address" />
          </Col>
        </FormGroup>
        </div>
        <div className="col-6">
        </div>
        <div className="col-12">
        <FormGroup row>
          <Label for="exampleText" lg={2}>Text Area</Label>
          <Col sm={10}>
            <Input type="textarea" name="text" id="message" />
          </Col>
        </FormGroup>
        </div>
        <Button>Send Email</Button>
        </div>
      </Form>
      </div>
    )
  }
}


export default ContactRentPoints;