import React, { Component } from 'react';
import './ViewCar.scss';
import { Card, CardImg, CardText, CardBody } from 'reactstrap';
import { Link } from "react-router-dom";


class ViewCar extends Component {

  render() {
    const car = this.props.location.state.car
    return(
      <div className="container text-center">
        <h1>{car.manufacturer} {car.model}</h1>
        <div>
      <Card className="car-card">
        <CardImg className="car-img" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4yJufFKpeT6b6jQ7AAc8ZL3HSTZpg6dVmhFYFVj8ztz6pmL1o" alt="car-logo" />
        <hr/>
        <CardBody>
          <CardText>Fuel type: {car.fuelType}</CardText>
          <CardText>Fuel Gear Box: {car.gearBoxType}</CardText>
          <CardText>Fuel Number of doors: {car.doorNumber}</CardText>
          <CardText>Fuel Number of seats: {car.seatNumber}</CardText>
          <CardText>Extra features: {car.extraFeatures}</CardText>
          <Link to={{
													pathname:`/cars/${car.id}/rent`,
													state: {car: car}
											}}>Rent</Link>
        </CardBody>
      </Card>
    </div>
      </div>
    )
  }
}

export default ViewCar;
