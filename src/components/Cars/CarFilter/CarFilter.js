import React, { Component } from 'react';
import { DropdownToggle, DropdownMenu, DropdownItem, UncontrolledButtonDropdown, FormGroup, Label, Input, Button } from 'reactstrap';
import './CarFilter.scss';

class CarFilter extends Component {
  render() {
    return (
      <div>
        <h3>
          Filter cars:
        </h3>
        <div className="row">
          <div className="col-12 filter-button">
            <UncontrolledButtonDropdown>
              <DropdownToggle caret size="md">
                Car Type
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={()=>this.props.filterByParams({bodyWorkType: 'SUV'})}>SUV</DropdownItem>
                <DropdownItem onClick={()=>this.props.filterByParams({bodyWorkType: 'hatchback'})}>Hatchback</DropdownItem>
                <DropdownItem onClick={()=>this.props.filterByParams({bodyWorkType: 'sedan'})}>Sedan</DropdownItem>
                <DropdownItem onClick={()=>this.props.filterByParams({bodyWorkType: 'coupe'})}>Coupe</DropdownItem>
                <DropdownItem onClick={()=>this.props.filterByParams({bodyWorkType: 'convertible'})}>Convertible</DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </div>
          <div className="col-12 filter-button">
          <UncontrolledButtonDropdown>
              <DropdownToggle caret size="md">
              Gear Box
              </DropdownToggle>
              <DropdownMenu>
              <DropdownItem onClick={()=>this.props.filterByParams({gearBoxType: 'mechanical'})}>Mechanical</DropdownItem>
              <DropdownItem onClick={()=>this.props.filterByParams({gearBoxType: 'automatic'})}>Automatic</DropdownItem>
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </div>
          <div className="col-12 filter-button">
        <FormGroup>
          <legend>Car Doors</legend>
          <FormGroup check>
            <Label>
              <Input onClick={() => this.props.removeFilterByParams('doorNumber')}  type="radio" name="radio1" />{' '}
              All
            </Label>
          </FormGroup>
          <FormGroup check>
            <Label check>
              <Input onClick={() => this.props.filterByParams({doorNumber: '5'})} type="radio" name="radio1" />{' '}
              5+
            </Label>
          </FormGroup>
        </FormGroup>
          </div>
          <div className="col-12 filter-button">
        <FormGroup>
          <legend>Seat Number</legend>
          <FormGroup check>
            <Label>
              <Input onClick={() => this.props.removeFilterByParams('seatNumber')}  type="radio" name="radio2" />{' '}
              All
            </Label>
          </FormGroup>
          <FormGroup check>
            <Label check>
              <Input onClick={() => this.props.filterByParams({seatNumber: '7'})} type="radio" name="radio2" />{' '}
              7+
            </Label>
          </FormGroup>
        </FormGroup>
          </div>
          <div className="col-12 filter-button">
          <Button onClick={() => this.props.resetFilter()}>
            Reset Filter
          </Button>
          </div>
      </div>
    </div>
    )
  }
}


export default CarFilter;