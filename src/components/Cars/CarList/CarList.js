import React, { Component } from 'react';
import { Card, CardBody, CardTitle} from 'reactstrap';
import { Link } from "react-router-dom";






class CarList extends Component {

 render() {

    return (
			<div>
      	<div className="row">
        	{this.props.cars.map((car, id) => (
					 	<div key={id} className="col-4 mb-4">
					 		<Card>
								<CardBody>
							 		<CardTitle>{car.manufacturer} {car.model} {car.makeYear}</CardTitle>
						 			</CardBody>
						 			<img width="100%" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4yJufFKpeT6b6jQ7AAc8ZL3HSTZpg6dVmhFYFVj8ztz6pmL1o" alt="car-img" />
						 			<CardBody>
											<div>
												<Link to={{
													pathname:`/cars/${car.id}`,
													state: {car: car}
											}}>View</Link>
											</div>
						 			</CardBody>
					 			</Card>
				 			</div>
							))}
      			</div>
					</div>
    		)
 			}
		}


export default CarList;
