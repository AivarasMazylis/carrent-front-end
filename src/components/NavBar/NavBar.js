import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  } from 'reactstrap';
  import { Link } from 'react-router-dom';
  import './NavBar.scss';
  import lexus from '../../lexus.svg';
  import auth0Client from '../../Auth/Auth';
 


class Example extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  

  render() {
    
    const signOut = () => {
      auth0Client.signOut();
      this.props.history.replace('/');
    };
  
    return (
      <div>
        <Navbar className="nav-header" light expand="md">
        <div className="container">
          <NavbarBrand className="header-logo"><img className="main-logo" src={lexus} alt="logo"></img></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <NavItem>
                  <Link to="/cars" className="nav-link">
                  Cars
                </Link>
                  </NavItem>
                  <NavItem>
                  <Link to="/rent-points" className="nav-link">
                  Rent Points
                </Link>
                  </NavItem>
                  {
                   !auth0Client.isAuthenticated() &&
                   <button className="btn btn-dark" onClick={auth0Client.signIn}>Sign In</button>
                  }
                  {
                   auth0Client.isAuthenticated() &&
                  <div>
                    <label className="mr-2 text-white">{auth0Client.getProfile().name}</label>
                    <button className="btn btn-dark" onClick={() => {signOut()}}>Sign Out</button>
                   </div>
                  }
              </Nav>
            </Collapse>
          </div>
        </Navbar>
      </div>
    );
  }
}

export default Example;

