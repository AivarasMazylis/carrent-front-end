import React, { Component } from 'react';
import axios from 'axios';
import CarFilter from '../../components/Cars/CarFilter'
import CarList from '../../components/Cars/CarList'
import qs from 'query-string';

import './Cars.scss';



class Cars extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cars: [],
      error: undefined,
    }
    this.filterByParams = this.filterByParams.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
    this.removeFilterByParams = this.removeFilterByParams.bind(this);
  }



  componentDidMount() {
    this.fetchCars(this.props.location.search);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.search !== prevProps.location.search) {
      this.fetchCars(this.props.location.search);
    }
  }


  resetFilter(){
    this.props.history.push('/cars')
  }

  fetchCars(filterParams){
    const URL = filterParams
    ? `https://car-rent-backend.herokuapp.com/cars${filterParams}`
    : 'https://car-rent-backend.herokuapp.com/cars';
    axios
      .get(URL)
      .then(cars => {
     
        this.setState(state => ({
          cars: cars.data,
          error: undefined,
        }));
        this.matchState()
      })
      .catch(error => {
        this.setState(state => ({
          cars: state.cars,
          error: error.message,
        }))
      })
  }

  filterByParams(filter) {
    let queryParams = qs.parse(this.props.location.search);
    queryParams = Object.assign(queryParams, filter);
    this.props.history.push(`/cars?${qs.stringify(queryParams)}`);
  }

    removeFilterByParams(filter) {
      let queryParams = qs.parse(this.props.location.search);
      delete queryParams[filter];
      this.props.history.push(`/cars?${qs.stringify(queryParams)}`);
    }

    render() {
      return (
      <div className="container">
				<h1 className="text-center m-4">Car List</h1>
        <div className="row">
       
        <div className="col-3">
        <CarFilter filterByParams={this.filterByParams} removeFilterByParams={this.removeFilterByParams} resetFilter={this.resetFilter} />
        </div>
        <div className="col-9">
        <CarList cars={this.state.cars} />
        </div>
        </div>
      </div>
      
      )
    }
  }


  export default Cars;

 