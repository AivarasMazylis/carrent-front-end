import React, { Component } from 'react';
import axios from 'axios';
import RentPointsList from '../../components/RentPoints/RentPointsList'

class RentingPoints extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rentPoints: [],
      error: undefined,
    }
  }

  componentDidMount() {
    this.fetchRentPoints();
  }

  fetchRentPoints(){
    axios
      .get('https://car-rent-backend.herokuapp.com/rentPoints')
      .then(rentPoints => {
     
        this.setState(state => ({
          rentPoints: rentPoints.data,
          error: undefined,
        }));
        this.matchState()
      })
      .catch(error => {
        this.setState(state => ({
          rentPoints: state.rentPoints,
          error: error.message,
        }))
      })
  }

    render() {
      return(
      <div className="container">
				<h1>Renting Points</h1>
        <div>
          <RentPointsList rentPoints={this.state.rentPoints}/>
        </div>
      </div>
      )
    }
  }

  export default RentingPoints;