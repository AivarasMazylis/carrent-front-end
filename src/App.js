import React, { Component } from 'react';
import {  Router, Route, Switch} from "react-router-dom";
import './App.scss';

import NavBar from './components/NavBar';
import Home from './containers/Home';
import Cars from './containers/Cars';
import RentingPoints from './containers/RentingPoints';
import ViewCar from './components/Cars/ViewCar'
import CarRent from './components/Cars/CarRent'
import history from './history';
import Callback from './Callback/Callback';



class App extends Component {

  render(){
    return (
      <Router history={history} component={App}>
        <div>
          <NavBar />
            <Switch>
              <Route exact path="/"  component={Home} />
              <Route path="/cars/:id/rent" component={CarRent} />
              <Route path="/cars/:id" component={ViewCar} />
     
              <Route path="/cars" component={Cars} />
              <Route path="/rent-points" component={RentingPoints} />
              <Route component={NoMatch} />
              <Route exact path='/callback' component={Callback}/>
            </Switch>
        </div>
     </Router>
    );
  }
}


const NoMatch = ({ location }) => (
  <div>
    <h3>
      404 NOT FOUND
    </h3>
  </div>
);

export default App;